// Package main
package main

import (
	"gitlab.privy.id/privypass/privypass-oauth2-core-se/cmd"
)

func main() {
	cmd.Start()
}
